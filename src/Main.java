import java.util.Scanner;

public class Main {

    public static int distinct(int size, int a[]) {
        int b[]= new int[100];
        int count=0;
        for (int i=0; i<100; i++) b[i]=0;
        for (int i=0; i<size; i++) b[a[i]]++;
        for (int i=0; i<100; i++) if (b[i]!=0) count++;
        return count;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int size = s.nextInt();

        int a[]= new int[size];
        for(int i=0; i<size; i++) {
            a[i] = s.nextInt();
        }

        System.out.println(distinct(size, a));
    }
}
